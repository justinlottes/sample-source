# ABOUT THE PROJECT #

These files are excerpts from a larger project.  The project consists of a custom sensor/motor board, a low level C++ App to control the board, a NodeJS App that interacts with the lower level C++ App and turns the basic commands into more useful features.  Then a basic UI in Angular 7 sits on top of everything else.  Finally, a Rasberry Pi Zero connects to the custom board, runs the 2 apps and serves that UI page.

The user will connect an arbitrary number of sensors and motors, and then divide the sensors and motors into zones.  A zone is just a logical grouping of sensors and motors that are working together.  Users assign zone-roles to zones to make them do useful things, ie Get Sensor Readings.

# nodejs dir #
zone-manager.service.ts contains code for managing the zones on different devices, add/remove zones and then add/modify/remove zone-roles.  
The zone-role.ts class is the parent class for all zone-roles.  It has basic functionality for managing the configuration of the ZoneRole.  
The sensor-reading-zone-role file contains the logic for getting a sensor reading from a sensor.  It handles configuration updates by creating, modifying or destroying individual sensors as well as cleaning up everything if the role is destroyed.  

# cpp dir #
This application communicates with the NodeJS App via a simple REST service, and I use a library called Pistache to handle setting up the server.  
The RestAPIIF.h file contains some helper functions that will automatically pull out typed parameters from the URL and put into the endpoint's callback.  
The RestAPI.cpp file shows this in action.  RegisterEndpoint takes (as template arguments) an ordered list of the types of each parameter, a vector of strings representing the path (PARAMETER is a placehold for a parameter), the HTTP verb, and a callback taking the request, response, and the ordered list of typed parameters.