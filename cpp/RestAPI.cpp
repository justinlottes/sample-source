#include "RestApi.h"

#include "AccessoryIF.h"
#include "AccessoryConfigurationLibraryIF.h"
#include "AccessoryTemplate.h"
#include "AccessoryWarehouseIF.h"
#include "Command.h"
#include "ConfigFactory.h"
#include "Parse.h"
#include "SystemSetting.h"
#include "ToJSON.h"

#include <nlohmann/json.hpp>

using JSON = nlohmann::json;

RestApi::RestApi(const std::string& name, ConfigFactory& cf)
	:
	TLObject(name),
	RestApiIF(name, cf),
	cfg(cf.Get(name))
{
}

static JSON ToJSONLocal(const std::shared_ptr<AccessoryIF>& accessory, bool full)
{
	JSON out = {};
	
	if(!accessory)
	{
		out = nullptr;
	}
	else
	{
		out = JSON({
			{AccessoryTemplate::CommandKeys::SERIAL_NUMBER, accessory->GetSerialNumber()()},
		});
		
		if(full)
		{
			auto cmds = accessory->GetCommands();
			JSON cmdsJSON = {};
			for(auto cmd : cmds)
			{
				cmdsJSON.push_back(ToJSON(cmd));
			}

			out.push_back({AccessoryTemplate::Keys::COMMANDS, cmdsJSON});
			out.push_back({AccessoryTemplate::Keys::VIRTUAL_ACCESSORY, accessory->IsVirtual()});
		}
	}

	return out;
}

static std::list<std::string> ParseCSV(std::string csv)
{
	std::list<std::string> out;
	std::stringstream ss(csv);
	std::string token;
	while(std::getline(ss, token, ',')){out.push_back(token);}
	return out;
}

void RestApi::Initialize()
{
	cfg->configLib.Bind();
	cfg->accWarehouse.Bind();
	cfg->settingsRepo.Bind();

	PistacheLib::Initialize();

	RegisterEndpoint<>({"accessory"}, GET, [this](const auto& request, auto&& response)
	{
		bool full = request.GetQueryParameter("info", "sn") == "full";

		auto accs = cfg->accWarehouse->GetAllAccessories();
		JSON accsJSON = {};
		for(auto acc : accs)
		{
			accsJSON.push_back(ToJSONLocal(acc, full));
		}

		response.Respond(PistacheLib::Ok, JSON({{"accessory", accsJSON}}).dump());
	});

	RegisterEndpoint<std::string>({"accessory", PARAM}, GET, [this](const auto& /*request*/, auto&& response, auto accessory)
	{
		auto acc = cfg->accWarehouse->GetAccessory(accessory);
		response.Respond(PistacheLib::Ok, JSON({{"accessory", ToJSONLocal(acc, true)}}).dump());
	}, "");

	RegisterEndpoint<std::string, std::string>({"accessory", PARAM, "command", PARAM}, PUT, [this](const auto& request, auto&& response, 
		auto accessory, auto command)
	{
		auto acc = cfg->accWarehouse->GetAccessory(accessory);
		auto inputs = JSON::parse(request.GetBody(), nullptr, false);

		if(!acc)
		{
			response.Respond(PistacheLib::NotFound, JSON({}).dump());
		}
		else if(inputs.is_discarded())
		{
			response.Respond(PistacheLib::BadRequest, JSON({}).dump());
		}
		else
		{
			auto resp = std::make_shared<RestApi::Response>(std::move(response));
			auto cb = [resp](const AccessoryIF::CommandResponse& cResponse)
			{
				if(cResponse.success)
				{
					resp->Respond(PistacheLib::Ok, JSON({{"response", cResponse.output}}).dump());
				}
				else
				{
					resp->Respond(PistacheLib::NotFound, JSON({}).dump());
				}
			};
			
			acc->ExecuteCommand(Command(command, {}), inputs, std::move(cb));
		}
	}, "", "");

	RegisterEndpoint<>({"command"}, GET, [this](const auto& /*request*/, auto&& response)
	{
		response.Respond(PistacheLib::Ok, JSON({{"commands", cfg->configLib->GetCommands()}}).dump());
	});

	RegisterEndpoint<std::string>({"command", PARAM}, GET, [this](const auto& /*request*/, auto&& response, auto command)
	{
		response.Respond(PistacheLib::Ok, JSON({{"commands", cfg->configLib->GetCommand(command)}}).dump());
	}, "");

	RegisterEndpoint<std::string>({"repo", PARAM}, GET, [this](const auto& request, auto&& response, auto repo)
	{
		std::list<std::string> values = ParseCSV(request.GetQueryParameter("values", ""));

		std::string lookbackStopStr = request.GetQueryParameter("stop", "");
		std::string lookbackStartStr = request.GetQueryParameter("start", "");

		auto lookbackStop = TimeUtils::GetRTTime();
		auto lookbackStart = lookbackStop - 1000 * 60 * 60;//TODO - this needs to be NVM

		uint64_t start, stop;
		if(!lookbackStartStr.empty() && lookbackStopStr.empty()
			&& Parse::Parse(lookbackStopStr, 10, stop) && Parse::Parse(lookbackStartStr, 10, start))
		{
			lookbackStart = TimeUtils::Convert(TimeUtils::SystemTime(start));
			lookbackStop = TimeUtils::Convert(TimeUtils::SystemTime(stop));
		}

		JSON itemsOut = {};
		if(repo == "settings")
		{
			auto filter = RepositoryFilter<SystemSetting, TimeUtils::RTTime, MaxFilter>();
			if(!values.empty())
			{
				filter.SetKeys(values);
			}

			auto items = cfg->settingsRepo->GetItems(filter);
			for(auto item : items)
			{
				itemsOut.push_back(ToJSON(item));
			}
		}
		
		response.Respond(PistacheLib::Ok, JSON({{repo, itemsOut}}).dump());
	}, "");

	RegisterEndpoint<>({"status"}, GET, [this](const auto& request, auto&& response)
	{
		std::list<std::string> values = ParseCSV(request.GetQueryParameter("values", ""));
		std::list<std::string> reposList = ParseCSV(request.GetQueryParameter("repos", ""));
		std::set<std::string> repos;
		std::for_each(std::begin(reposList), std::end(reposList), [&repos](const auto& repo){repos.insert(repo);});

		LOG(logger, "Get status: " << " " << reposList.size());

		JSON out = {
		};

		if(reposList.empty())
		{
			reposList.push_back("settings");
		}

		for(auto repo : reposList)
		{
			if(repo == "settings")
			{
				auto filter = RepositoryFilter<SystemSetting, TimeUtils::RTTime, MaxFilter>();
				if(!values.empty())
				{
					filter.SetKeys(values);
				}

				auto items = cfg->settingsRepo->GetItems(filter);
				for(auto item : items)
				{
					out["settings"].push_back(ToJSON(item));
				}
			}
			else
			{
				out[repo].push_back(nullptr);
			}
		}

		response.Respond(PistacheLib::Ok, out.dump());
	});

	PistacheLib::Start();
}
