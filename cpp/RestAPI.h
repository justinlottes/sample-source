#pragma once

#include "Deferred.h"
#include "HierarchyObject.h"
#include "PistacheLib.h"
#include "RestApiIF.h"
#include "TLObject.h"

class RestApi : public HierarchyObject<TLObject, RestApiIF<PistacheLib>>
{
public:

	class Config
	{
	public:

		Deferred<std::shared_ptr<class AccessoryConfigurationLibraryIF>> configLib;
	};

	RestApi(const std::string& name, class ConfigFactory& cf);

	virtual void Initialize() override;


private:

	std::shared_ptr<Config> cfg;
	
};
