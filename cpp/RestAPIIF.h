#pragma once

#include "ConfigFactory.h"
#include "RTAssert.h"

#include <algorithm>
#include <functional>
#include <utility>
#include <vector>

namespace RestApiIFCB
{
	template<class LibIF, class... Args>
	struct CallbackT
	{
		using Callback = std::function<void(const typename LibIF::Request&, typename LibIF::Response&&, const Args&...)>;
	};

	template<class LibIF>
	struct CallbackT<LibIF, void>
	{
		using Callback = std::function<void(const typename LibIF::Request&, typename LibIF::Response&&)>;
	};
}

template<class LibIF>
class RestApiIF : public LibIF
{
public:

	static const std::string PARAM;

	RestApiIF();
	RestApiIF(const std::string& name, class ConfigFactory& cf);

	template<class... Args>
	using Callback = typename RestApiIFCB::CallbackT<LibIF, Args...>::Callback;

	template<class... Args>
	void RegisterEndpoint(std::vector<std::string> endpoint, typename LibIF::RequestType requestType, const Callback<Args...>&& callback,
		const typename std::decay<Args>::type&... defaultValues);


private:

	template<std::size_t I, class Data>
	std::function<void(typename std::decay<std::tuple_element_t<I, Data>>::type&)> CreateFunc(const typename LibIF::Request& request);

	template<class Data, std::size_t... Is>
	bool ExtractParams(const typename LibIF::Request& request, Data& data, std::index_sequence<Is...> is);
	
	template<class Data, std::size_t... Is>
	void SendCallback(const Callback<typename std::tuple_element<Is, Data>::type...>&& callback, const typename LibIF::Request& request, 
		typename LibIF::Response&& response, const Data& data, std::index_sequence<Is...> is);
};

template<class LibIF>
const std::string RestApiIF<LibIF>::PARAM = ":";

template<class LibIF>
RestApiIF<LibIF>::RestApiIF()
	:
	LibIF("", * new ConfigFactory())
{
	RTASSERT(false);//DO NOT USE THIS
}

template<class LibIF>
RestApiIF<LibIF>::RestApiIF(const std::string& name, class ConfigFactory& cf)
	:
	LibIF(name, cf)
{
}

template<class LibIF>
template<class... Args>
void RestApiIF<LibIF>::RegisterEndpoint(std::vector<std::string> endpoint, typename LibIF::RequestType requestType, 
	const Callback<Args...>&& callback, const typename std::decay<Args>::type&... defaultValues)
{
	//std::ostringstream uriTemplate;//TODO - wtf, compiler bug???
	std::string uriTemplate;
	size_t params = 0;
	std::for_each(std::begin(endpoint), std::end(endpoint), [&uriTemplate, &params](const auto& e)
	{
		uriTemplate += "/" + e;
		if(e == PARAM)
		{
			uriTemplate += std::to_string(params);
			++params;
		}
	});

	RTASSERT(params == sizeof...(Args));

	LOG(LibIF::logger, "register endpoint: " << uriTemplate << " " << std::to_string(params));

	LibIF::RegisterEndpoint(uriTemplate, requestType,
		[this, endpoint, callback = std::move(callback), defaultValues...](const typename LibIF::Request& request, typename LibIF::Response&& response)
	{
		std::tuple<typename std::decay<Args>::type...> values = std::make_tuple(defaultValues...);

		ExtractParams(request, values, std::make_index_sequence<sizeof...(Args)>());

		SendCallback(std::move(callback), request, std::move(response), values, std::make_index_sequence<sizeof...(Args)>());
	});
}

template<class LibIF>
template<std::size_t I, class Data>
std::function<void(typename std::decay<std::tuple_element_t<I, Data>>::type&)> RestApiIF<LibIF>::CreateFunc(const typename LibIF::Request& request)
{
	return [request](typename std::decay<std::tuple_element_t<I, Data>>::type& value)
	{
		value = request.template GetParameterAt<typename std::tuple_element_t<I, Data>>(I, value);
	};
}

template<class LibIF>
template<class Data, std::size_t... Is>
bool RestApiIF<LibIF>::ExtractParams(const typename LibIF::Request& request, Data& data, std::index_sequence<Is...>)
{
	auto funcs = std::make_tuple(CreateFunc<Is, Data>(request)...);
	
	int dummy[] = {0, ((std::get<Is>(funcs))(std::get<Is>(data)), void(), 0)...};
	(void) dummy;
	(void) funcs;
	return true;
}

template<class LibIF>
template<class Data, std::size_t... Is>
void RestApiIF<LibIF>::SendCallback(const Callback<typename std::tuple_element<Is, Data>::type...>&& callback, const typename LibIF::Request& request, 
	typename LibIF::Response&& response, const Data& data, std::index_sequence<Is...>)
{
	callback(request, std::move(response), std::get<Is>(data)...);
}
