import { ClientError } from 'utils/client-error';
import { DeviceAccessory } from 'models/device-accessory.model';
import { SensorReading } from '../utils/sensor-reading';
import { SerialNumber } from 'utils/serial-number';
import { Zone } from 'zone-utils/zone';
import { ZoneRole } from 'zone-utils/zone-role';
import { ZoneRoleConfigChange } from 'zone-utils/zone-role-config-change';

import DeviceCommsService from 'services/device-comms.service';
import DeviceCommandsService from 'services/device-commands.service';
import RepoService from 'services/repo.service';
import EventsService from 'services/events.service';

class Command {

	constructor(cfg: any) {
		this.name = cfg.name;
		this.parameters = cfg.parameters;
	}

	name!: string;
	parameters!: any;
};

class SensorConfig {
	serialNumber!: SerialNumber;
	initializationCommand!: Command;
	startReadingsCommand!: Command
	getReadingCommandArgs!: any;
	stopReadingsCommand!: Command;
	period!: number;
	enabled!: boolean;
	timeout!: NodeJS.Timeout | null;
};

export class SensorReadingZoneRole extends ZoneRole {

	private sensors = new Map<SerialNumber, SensorConfig>();

	constructor(name: string, zone: Zone) {
		super(name, zone);
	}

	async updateConfigurationImpl(config: any, configChange: ZoneRoleConfigChange): Promise<void> {
        //TODO - handle erros initializing
		return Promise.resolve().then(() => {
			let arr: Promise<unknown>[] = new Array();

			if(configChange.delete) {
				configChange.delete.forEach((del: string) => {
					arr.push(Promise.resolve().then(() => {
						let sensorInfo = this.sensors.get(new SerialNumber(del))
						if(sensorInfo) {
							if(sensorInfo.timeout) {
								clearTimeout(sensorInfo.timeout)
							}

							if(sensorInfo.enabled && sensorInfo.stopReadingsCommand) {
								arr.push(DeviceAccessory.findOne({
									where: {
										deviceId: this.zone.device.id,
										serialNumber: del,
									}
								}).then((accessory: DeviceAccessory | null) => {
									if(accessory) {
										sensorInfo = sensorInfo as SensorConfig;
										arr.push(DeviceCommsService.sendCommand(this.zone.device, accessory, sensorInfo.stopReadingsCommand.name,
											sensorInfo.stopReadingsCommand.parameters));
									} else {
										//TODO - execute later???
										console.error('Accessory does not exist: ' + this.zone.device.serialNumber.toString() + ':' + sensorInfo?.serialNumber.toString());
									}
								}));
							}

							sensorInfo.enabled = false;
						}
					}));
				});
			}

			let noCommand = null as unknown as Command;

			if(configChange.put) {
				Object.keys(configChange.put).forEach((key: string) => {
					let sensorInfo = this.sensors.get(new SerialNumber(key));
					if(sensorInfo) {
						throw new ClientError('TODO - sensorInfo already exists: ' + key);
					} else {
						let cfg = new SensorConfig();	
						cfg.serialNumber = new SerialNumber(key);
						cfg.initializationCommand = configChange.put[key].initializationCommand ? 
							new Command(configChange.put[key].initializationCommand) : noCommand;
						cfg.startReadingsCommand = configChange.put[key].startReadingsCommand ?
							new Command(configChange.put[key].startReadingsCommand) : noCommand;
						cfg.getReadingCommandArgs = configChange.put[key].getReadingCommandArgs;
						cfg.stopReadingsCommand = configChange.put[key].stopReadingsCommand ?
							new Command(configChange.put[key].stopReadingsCommand) : noCommand;
						cfg.period = configChange.put[key].period;
						cfg.enabled = configChange.put[key].enabled;;
						cfg.timeout = null;

						this.sensors.set(cfg.serialNumber, cfg);
						this.getReading(cfg);
					}
				});
			}
			
			if(configChange.patch) {
				Object.keys(configChange.patch).forEach((key: string) => {
					let sensorInfo = this.sensors.get(new SerialNumber(key));
					if(sensorInfo) {
						if(sensorInfo.enabled != configChange.patch[key].enabled) {
							sensorInfo.enabled = configChange.patch[key].enabled;
							if(sensorInfo.enabled && sensorInfo.startReadingsCommand) {
								arr.push(DeviceAccessory.findOne({
									where: {
										deviceId: this.zone.device.id,
										serialNumber: key,
									}
								}).then((accessory: DeviceAccessory | null) => {
									if(accessory) {
										arr.push(DeviceCommsService.sendCommand(this.zone.device, accessory, sensorInfo?.startReadingsCommand!.name, sensorInfo?.startReadingsCommand.parameters))
									} else {
										console.error('accessory does not exist: ' + this.zone.device.serialNumber.toString() + ':' + sensorInfo?.serialNumber.toString())
									}
								}));
							} else if(!sensorInfo.enabled && sensorInfo.stopReadingsCommand) {
								arr.push(DeviceAccessory.findOne({
									where: {
										deviceId: this.zone.device.id,
										serialNumber: key,
									}
								}).then((accessory: DeviceAccessory | null) => {
									if(accessory) {
										arr.push(DeviceCommsService.sendCommand(
											this.zone.device, accessory, sensorInfo?.stopReadingsCommand!.name, sensorInfo?.stopReadingsCommand.parameters));
									} else {
										//TODO - queue up for later???
										console.error('accessory does not exist: ' + this.zone.device.serialNumber.toString() + ':' + sensorInfo?.serialNumber.toString());
									}
								}));
							};
						}
					} else {
						throw new ClientError('Existing configuration does not exist: ' + key);
					}
				});
			}

			return arr.reduce((prev: Promise<unknown>, current: Promise<unknown>) => {return prev.then(() => current); }, Promise.resolve()).then(() => {});
		});
	}


	async destroyImpl(): Promise<any> {
		let deleteConfig = new ZoneRoleConfigChange();
		deleteConfig.delete = Array.from(this.sensors).map((sensorInfo: [SerialNumber, SensorConfig]) => {
			return sensorInfo[0].toString();
		});

		return this.updateConfigurationImpl({}, deleteConfig);
	}

	private getReading(sensorConfig: SensorConfig) {
		if(sensorConfig.enabled) {
			DeviceAccessory.findOne({
				where: {
					deviceId: this.zone.device.id,
					serialNumber: sensorConfig.serialNumber.toString(),
				}
			}).then((deviceAccessory: DeviceAccessory | null) => {
				if(deviceAccessory) {
					return DeviceCommandsService.getSensorReading(this.zone.device, deviceAccessory, sensorConfig.getReadingCommandArgs).then((value: SensorReading) => {
						RepoService.addItem(value);
						EventsService.notifyEvent('DEVICE', 'SensorReadingEvent', new Map(Object.keys(value).map(key => [key, (value as any)[key]])));
					}).catch((err: any) => {
                        console.error('error getting reading: ' + JSON.stringify(err));
                        if(err.stacktrace) {
                            console.error(JSON.stringify(err.stacktrace));
                        }

                        this.getReading(sensorConfig);
                    });
				}
			}).then(() => {
				sensorConfig.timeout = setTimeout(() => {
					this.getReading(sensorConfig);
				}, sensorConfig.period);
			});
		}
	}
};