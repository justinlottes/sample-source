import { Consts } from 'app/consts';
import { ClientError } from 'utils/client-error';
import { Device } from 'models/device.model';
import { DeviceSetting } from 'models/device-setting.model';
import { OptionalTransaction } from 'utils/optional-transaction';
import { Zone } from 'zone-utils/zone';
import { ZoneConsts } from 'zone-utils/zone-consts';
import { ZoneRole } from 'zone-utils/zone-role';
import { ZoneRoleConfigChange } from 'zone-utils/zone-role-config-change';

import DeviceConfigurationService from 'services/device-configuration.service';
import DeviceRegistrationService from 'services/device-registration.service';

class ZoneManagerService {

	public readonly ZONE_CREATED = 'CREATED';
	public readonly ZONE_DESTROYED = 'DESTROYED';
	public readonly ZONE_ROLE_CREATED = 'CREATED';
	public readonly ZONE_ROLE_REMOVED = 'REMOVED';

	private zones = new Map<string, Map<string, Zone>>();

	constructor() {
		DeviceRegistrationService.registerPersistentDeviceSetting(Consts.DeviceSettings.ZONES);
		DeviceRegistrationService.getDevicesObservable().subscribe((device: Device) => this.loadDevice(device));
	}

	async getZones(device: Device, detail: 'ZONES') : Promise<Array<string>>;
	async getZones(device: Device, detail: 'ZONE_ROLES') : Promise<any>; 
	async getZones(device: Device, detail: 'CONFIGURATION') : Promise<any>;
	async getZones(device: Device, detail: 'ZONES' | 'ZONE_ROLES' | 'CONFIGURATION') : Promise<Array<string> | any> {
		let zones = this.zones.get(device.serialNumber.toString());
		if(zones) {
			if(detail === 'ZONES') {
				return Promise.resolve(Array.from(zones.keys()));
			} else if(detail === 'ZONE_ROLES') {
				return Promise.resolve(Array.from(zones).reduce((obj: any, zone: [string, Zone]) => {
					obj[zone[0]] = Array.from(zone[1].roles).map((role => role[0]));
					return obj;
				}, {} as any));
			} else if(detail === 'CONFIGURATION') {
				return Promise.resolve(this.objectifyMap(new Map(Array.from(zones).map((zoneInfo: [string, Zone]) => {
					return [
						zoneInfo[1].name,
						this.mapifyZoneRoles(zoneInfo[1]),
					]
				}))));
			} else {
				throw new ClientError('BAD_DETAIL_TYPE');
			}
		} else {
			throw new ClientError(ZoneConsts.Error.DEVICE_DOES_NOT_EXIST);
		}
	}

	async addZone(device: Device, zoneName: string, transaction: OptionalTransaction | null) : Promise<Zone> {
		let zone = this.getZone(device, zoneName);
		if(zone) {
			throw new ClientError(ZoneConsts.Error.ZONE_ROLE_EXISTS);
		} else {
			zone = new Zone(device, zoneName);
			this.setZone(zone);

			return this.commitZones(device, transaction).then(() => zone as Zone);
		}
	}

	async removeZone(device: Device, zoneName: string, transaction: OptionalTransaction | null) {
		return this.destroyZone(device, zoneName).then((success: boolean) => {
			if(success) {
				return this.commitZones(device, transaction);
			} else {
				throw new ClientError(ZoneConsts.Error.ZONE_DOES_NOT_EXIST);
			}
		});
	}

	async configureRole(device: Device, zoneName: string, roleName: string, roleConfig: ZoneRoleConfigChange, transaction: OptionalTransaction | null) {
		let zone = this.getZone(device, zoneName);
		if(zone) {
			let zoneRole = zone.roles.get(roleName);
			if(!zoneRole) {
				zoneRole = ZoneRole.createZoneRole(roleName, zone);
				zone.roles.set(roleName, zoneRole);
			}

			return zoneRole.updateConfiguration(roleConfig).then(() => {
				return this.commitZones(device, transaction);
			});
		} else {
			throw new ClientError(ZoneConsts.Error.ZONE_DOES_NOT_EXIST);
		}
	}

	async removeRole(device: Device, zoneName: string, roleName: string, transaction: OptionalTransaction | null) {
		let zone = this.getZone(device, zoneName);
		if(zone) {
			let zoneRole = zone.roles.get(roleName);
			if(zoneRole) {
				zone.roles.delete(roleName);
				return zoneRole.destroy().then(() => {
					return this.commitZones(device, transaction);
				});
			} else {
				throw new ClientError(ZoneConsts.Error.UNKNOWN_ZONE_ROLE);
			}
		} else {
			throw new ClientError(ZoneConsts.Error.ZONE_ROLE_DOES_NOT_EXIST);
		}
	}

	private getZone(device: Device, zoneName: string) {
		let zones = this.zones.get(device.serialNumber.toString());
		if(zones) {
			return zones.get(zoneName);
		}
	}

	private setZone(zone: Zone) {
		let zones = this.zones.get(zone.device.serialNumber.toString());
		if(!zones) {
			zones = new Map<string, Zone>();
			this.zones.set(zone.device.serialNumber.toString(), zones);
		}

		zones.set(zone.name, zone);
	}

	private async destroyZone(device: Device, zoneName: string) {
		let zone = this.getZone(device, zoneName);
		if(zone) {
			this.zones.get(device.serialNumber.toString())?.delete(zoneName);
			return Promise.all(Array.from(zone.roles).map((zoneRole: [string, ZoneRole]) => zoneRole[1].destroy())).then(() => true);
		} else {
			return false;
		}
	}

	private commitZones(device: Device, transaction: OptionalTransaction | null) {
		let deviceZones = Array.from(this.zones.get(device.serialNumber.toString()) as Map<string, Zone>);
		let formattedZones = this.objectifyMap(new Map(Array.from(deviceZones).map((zoneInfo: [string, Zone]) => {
			return [
				zoneInfo[1].name,
				this.mapifyZoneRoles(zoneInfo[1]),
			]
		})));

		return DeviceConfigurationService.setDeviceConfiguration(device, 
			[{name: Consts.DeviceConfiguration.ZONES, value: JSON.stringify(formattedZones)}], transaction).then(() => {});
	}

	private mapifyZoneRoles(zone: Zone) {
		return new Map(Array.from(zone.roles).map((entry: [string, ZoneRole]) => {
			return [entry[0], entry[1].getConfiguration()];
		}));
	}

	private objectifyMap(map: any) {
		if(map instanceof Map) {
			return Array.from(map).reduce((object: any, entry: any) => {
				object[entry[0]] = this.objectifyMap(entry[1]);
				return object;
			}, {} as any);
		} else {
			return map;
		}
	}

	private async loadDevice(device: Device) {
		return Promise.resolve(() => {
			let existingZones = this.zones.get(device.serialNumber.toString());
			if(existingZones) {
				return Promise.all(Array.from(existingZones).map((zoneInfo: [string, Zone]) => {
					return Promise.all(Array.from(zoneInfo[1].roles).map((zoneRole: [string, ZoneRole]) => zoneRole[1].destroy()));
				}));
			}
		}).then(() => {
			return DeviceConfigurationService.getDeviceConfiguration(device, Consts.DeviceConfiguration.ZONES, null)
		}).then((setting: DeviceSetting | null) => {
			this.zones.set(device.serialNumber.toString(), new Map<string, Zone>());
			if(setting) {
				let deviceConfig: any = {};
				try{ 
					deviceConfig = JSON.parse(setting.value);
				} catch(ex) {
				}

				return Promise.all(Object.keys(deviceConfig).map((zoneName: string) => {
					let zoneConfig = deviceConfig[zoneName];

					let zone = new Zone(device, zoneName);
					this.setZone(zone);

					return Promise.all(Object.keys(zoneConfig).map((zoneRoleName: string) => {
						let zoneRole = ZoneRole.createZoneRole(zoneRoleName, zone);
						let cfgChange = new ZoneRoleConfigChange();
						cfgChange.put = zoneConfig[zoneRoleName];
						return zoneRole.updateConfiguration(cfgChange).then(() => {
							zone.roles.set(zoneRoleName, zoneRole);
						});
					}));
				}));
			};
		}).then(() => {});
	}
};

export default new ZoneManagerService();