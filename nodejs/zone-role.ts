import { Observable, Subject } from 'rxjs';
const { Sema } = require('async-sema');

import { ClientError } from 'utils/client-error';
import { Zone } from 'zone-utils/zone';
import { ZoneConsts } from 'zone-utils/zone-consts';
import { ZoneRoleConfigChange } from 'zone-utils/zone-role-config-change';

class EventInfo {
	name!: string;
	event!: any;
}

export abstract class ZoneRole {

	private static readonly zoneRoleMap = new Map<string, (zone: Zone) => ZoneRole>();

	readonly name: string;
	readonly zone: Zone;
	private configuration: any;

	private eventSubjects = new Map<string, Subject<EventInfo>>();
	private sema = new Sema(1, {
		capacity: 3,
	});

	constructor(name: string, zone: Zone) {
		this.name = name;
		this.zone = zone;
	}

	static registerZoneRole<ZR extends ZoneRole>(name: string, type: new(name: string, zone: Zone) => ZR) {
		if(this.zoneRoleMap.has(name)) {
			console.assert('cannot add duplicate zone name: ' + name);
		} else {
			this.zoneRoleMap.set(name, (zone: Zone) => {
				return new type(name, zone);
			});
		}
	}

	static createZoneRole(name: string, zone: Zone) {
		let builder = this.zoneRoleMap.get(name);
		if(builder) {
			return builder(zone);
		} else {
			throw new ClientError(ZoneConsts.Error.UNKNOWN_ZONE_ROLE)
		}
	}

	abstract async updateConfigurationImpl(config: any, configChange: ZoneRoleConfigChange): Promise<void>;
	protected abstract async destroyImpl(): Promise<void>;

	getEventObservable(eventName: string): Observable<EventInfo> {
		let subject = this.eventSubjects.get(eventName);
		if(!subject) {
			subject = new Subject<EventInfo>();
			this.eventSubjects.set(eventName, subject);
		}

		return subject.asObservable();
	}

	protected emitEvent(eventName: string, event: any) {
		let subject = this.eventSubjects.get(eventName);
		if(subject) {
			subject.next({name: eventName, event: event});
		}
	}

	async destroy(): Promise<void> {
		return this.destroyImpl().then(() => {
			this.eventSubjects.forEach((subject: Subject<EventInfo>) => {
				subject.complete();
			});

			this.eventSubjects.clear();
		});
	}
	
	async updateConfiguration(configChange: ZoneRoleConfigChange): Promise<void> {
		return this.sema.acquire().then(() => {
			let newConfig = Object.assign({}, this.configuration);
			if(configChange.delete) {
				configChange.delete.forEach((key: string) => {
					delete newConfig[key];
				});
			}

			if(configChange.put) {
				newConfig = Object.assign(newConfig, configChange.put);
			}

			if(configChange.patch) {
				let isPrimitive = (x: any) => {
					return (x !== Object(x));
				};

				let patchObject = (object: any, patch: any) => {
					Object.keys(patch).forEach((key: string) => {
						if(!object[key] || isPrimitive(object[key]) || isPrimitive(patch[key])) {
							object[key] = patch;
						} else {
							patchObject(object[key], patch[key]);
						}
					});
				};

				newConfig = patchObject(newConfig, configChange.patch);
			}

			return this.updateConfigurationImpl(Object.assign({}, newConfig), configChange).then(() => {
				this.configuration = newConfig;
			});
		}).finally(() => {
			this.sema.release();
		});
	}

	getConfiguration() {
		return Object.assign({}, this.configuration);
	}
};